# Would/Could/Should do's

I built the components in the kind of way I like to consume components - mostly stateless - but I'm not married to how I build components.
The table-app component itself could be reusable if we passed in how to retrieve transactions, and it could then actually live in `task-ui`

* `import` modules
* Make 'search' function decoupled from name and partially apply to create `search-transactions-by-name` function i.e.

```js
const searchTransactionsByName = search("name")(value, transactions);
```

* Make 'sort' function decoupled from amount and partially apply to create `search-transactions-by-name` function e.g.

```js
const sortTransactionsByAmount = sort("amount")(order, transactions); //Simplified example
```

* Seperate git repo's for libraries (Had some weirdness with Jest and nested `.babelrc`'s')
* Prop types
* Column order props for transaction table
* Memoize search, sort and processTransactions operations
* Perhaps add more classes to components for consumers to develop styles more / style the component more out of the box to be more oppinionated (depends on requirements).
* Use Redux :)
* And of course I'm open to receiving any feedback on my components.
