const React = require('react')
const TransactionTable = require('../../task-ui/transaction-table')
const TransactionClient = require('../transaction-client')
const { sortTransactionsByAmount, searchTransactionsByName } = require('../../task-library')
require('./style.scss')
const config = {
  transactionsBaseUrl: 'http://localhost:3000' // Add env to build process
}

const transactionClient = TransactionClient({ fetch, baseUrl: config.transactionsBaseUrl })

const processTransactions = (search, sort, transactions) => {
  const filtered = searchTransactionsByName(search, transactions)
  return sort ? sortTransactionsByAmount(sort, filtered) : filtered
}

class TableApp extends React.Component {
  componentDidMount () {
    this.setState(state => Object.assign(state || {}, {
      headers: [
        { text: 'ID' },
        { text: 'Amount', onSortClick: this.handleSortAmount.bind(this) },
        { text: 'Date' },
        { text: 'Business' },
        { text: 'Name' },
        { text: 'Type' },
        { text: 'Account' }
      ]
    }))

    transactionClient.getTransactions().then(transactions => {
      this.setState(state => Object.assign(
        state,
        {
          presentationTransactions: transactions,
          transactions
        }))
    })
  }
  setTransactions (transactions) {
    this.setState(state => Object.assign(state, { transactions }))
  }
  handleFilter (e) {
    const search = e.target.value
    const sort = this.state.headers[1].sort

    const presentationTransactions = processTransactions(search, sort, this.state.transactions)

    this.setState(state => Object.assign(state, { presentationTransactions, search }))
  }
  handleSortAmount () {
    const sort = ~this.state.headers[1].sort ? -1 : 1
    const search = this.state.search
    const headers = this.state.headers.map(h => Object.assign({}, h))
    headers[1].sort = sort

    const presentationTransactions = processTransactions(search, sort, this.state.transactions)

    this.setState(state => Object.assign(state, { presentationTransactions, headers }))
  }
  render () {
    return (
      <div className='transaction-table-app'>
        {
          (this.state &&
          this.state.presentationTransactions)
            ? (
              <div>
                <div className='search-bar'>
                  <label htmlFor='search-by-name'>Search by name: </label>
                  <input id='search-by-name' type='text' onChange={this.handleFilter.bind(this)} />
                </div>
                <TransactionTable headers={this.state.headers} data={this.state.presentationTransactions} />
              </div>
            )
            : 'Loading'
        }
      </div>
    )
  }
}

module.exports = TableApp
