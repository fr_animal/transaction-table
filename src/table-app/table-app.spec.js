/* eslint-env jest */
const React = require('react')
const { mount } = require('enzyme')
const { helpers } = require('faker')

const TableApp = require('./')

describe('<TableApp />', () => {
  const transactions = require('./fixtures/transactions.json')

  it('Should mount', () => {
    fetch.once(JSON.stringify(transactions))
    mount(<TableApp />)
  })

  it('Should render loading', () => {
    fetch.once(JSON.stringify(transactions))

    const table = mount(<TableApp />)

    expect(table.html()).toMatchSnapshot()
  })

  it('Should render table', done => {
    fetch.once(JSON.stringify(transactions))
    const table = mount(<TableApp />)

    setTimeout(() => {
      expect(table.html()).toMatchSnapshot()
      done()
    })
  })

  it('Should sort transactions', done => {
    fetch.once(JSON.stringify(transactions))
    const table = mount(<TableApp />)
    setTimeout(() => {
      table.update()
      const sortButton = table.find('button')
      sortButton.simulate('click')
      expect(table.html()).toMatchSnapshot()
      done()
    })
  })

  it('Should sort transactions both ways', done => {
    fetch.once(JSON.stringify(transactions))
    const table = mount(<TableApp />)
    setTimeout(() => {
      table.update()
      const sortButton = table.find('button')
      sortButton.simulate('click')
      sortButton.simulate('click')
      expect(table.html()).toMatchSnapshot()
      done()
    })
  })

  it('Should search by name', done => {
    fetch.once(JSON.stringify(transactions))
    const table = mount(<TableApp />)
    setTimeout(() => {
      table.update()
      const searchInput = table.find('input')
      searchInput.simulate('change', { target: { value: 'money market' } })
      expect(table.html()).toMatchSnapshot()
      done()
    })
  })

  it('Should search by name and filter', done => {
    fetch.once(JSON.stringify(transactions))
    const table = mount(<TableApp />)
    setTimeout(() => {
      table.update()
      const searchInput = table.find('input')
      const sortButton = table.find('button')

      searchInput.simulate('change', { target: { value: 'money market' } })
      sortButton.simulate('click')

      expect(table.html()).toMatchSnapshot()
      done()
    })
  })
})
