const React = require('react')
const ReactDOM = require('react-dom')
const TableApp = require('./table-app')

ReactDOM.render(<TableApp />, document.getElementById('root'))
