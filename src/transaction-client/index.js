module.exports = ({ fetch, baseUrl }) => ({
  getTransactions: () =>
    fetch(`${baseUrl}/transactions`).then(res => res.json())
})
