/* eslint-env jest */
const TransactionClient = require('./')

describe('TransactionClient()', () => {
  it('.getTransactions()', async () => {
    const baseUrl = 'http://test'
    const returnData = { some: 'data' }
    const jsonStub = jest.fn(() => Promise.resolve(returnData))
    const fetchStub = jest.fn(() => Promise.resolve({ json: jsonStub }))

    const transactionClient = TransactionClient({ fetch: fetchStub, baseUrl })

    const trasactions = await transactionClient.getTransactions()
    expect(fetchStub).toHaveBeenCalledWith('http://test/transactions')
    expect(trasactions).toEqual(returnData)
  })
})
