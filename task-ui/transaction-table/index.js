const customTable = require('../custom-table')

const transformData = data => data && data.map(t => [t.id, t.amount, t.date, t.business, t.name, t.type, t.account])
module.exports = customTable({ transformData })
