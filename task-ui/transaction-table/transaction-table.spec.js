/* eslint-env jest */
const React = require('react')
const TransactionTable = require('./')
const { shallow } = require('enzyme')

describe('TransactionTable', () => {
  it('should render', () => {
    shallow(<TransactionTable />)
  })

  it('should render headers', () => {
    const headers = [{ text: 'Some header' }, { text: 'Some other header' }]
    const transactionTable = shallow(<TransactionTable headers={headers} />)
    const table = transactionTable.find('Table')
    expect(table.prop('headers')).toEqual(headers)
  })

  it('should render transactions correctly', () => {
    const transactions = [
      {
        id: 1,
        amount: 472.71,
        date: '2012-02-02T00:00:00.000Z',
        business: 'Ortiz and Sons',
        name: 'Personal Loan Account 7277',
        type: 'withdrawal',
        account: '77845201'
      },
      {
        id: 2,
        amount: 28.37,
        date: '2012-02-02T00:00:00.000Z',
        business: 'Carroll LLC',
        name: 'Credit Card Account 5738',
        type: 'payment',
        account: '33724272'
      }
    ]

    const transactionTable = shallow(<TransactionTable data={transactions} />)

    const table = transactionTable.find('Table')
    const tableData = table.prop('data')
    expect(tableData).toEqual([
      [1, 472.71, '2012-02-02T00:00:00.000Z', 'Ortiz and Sons', 'Personal Loan Account 7277', 'withdrawal', '77845201'],
      [2, 28.37, '2012-02-02T00:00:00.000Z', 'Carroll LLC', 'Credit Card Account 5738', 'payment', '33724272']
    ])
  })
})
