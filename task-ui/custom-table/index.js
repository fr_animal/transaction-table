const React = require('react')
const Table = require('../table')

const customTable = ({ transformData = (data => data) }) => {
  class CustomTable extends React.Component {
    render () {
      return <Table headers={this.props.headers} data={transformData(this.props.data)} />
    }
  }
  return CustomTable
}

module.exports = customTable
