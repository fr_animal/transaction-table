/* eslint-env jest */
const React = require('react')
const { shallow } = require('enzyme')
const customTable = require('./')

describe('customTable()', () => {
  it('Should render', () => {
    const TransactionTable = customTable({})
    shallow(<TransactionTable />)
  })

  it('Should pass on data', () => {
    const data = [
      ['some', 'data', 'rows'],
      ['some1', 'data1', 'rows1']
    ]
    const TransactionTable = customTable({})
    const table = shallow(<TransactionTable data={data} />).find('Table')
    const dataProp = table.prop('data')
    expect(dataProp).toEqual(data)
  })

  it('Should transform data', () => {
    const data = [
      ['some', 'data', 'rows'],
      ['some1', 'data1', 'rows1']
    ]

    const transformData = data => [...data, ['some', 'last', 'col']]
    const TransactionTable = customTable({ transformData })
    const table = shallow(<TransactionTable data={data} />).find('Table')
    const dataProp = table.prop('data')
    expect(dataProp).toEqual([...data, ['some', 'last', 'col']])
  })

  it('Should render headers', () => {
    const headers = [{text: 'Header 1'}, {text: 'Header 1'}]
    const TransactionTable = customTable({})
    const table = shallow(<TransactionTable headers={headers} />).find('Table')
    const headersProp = table.prop('headers')
    expect(headersProp).toEqual(headers)
  })
})
