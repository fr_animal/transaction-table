const React = require('react')

const sortSymbolMap = {
  '1': '▲',
  '-1': '▼'
}

class Table extends React.Component {
  getSortSymbol (sort) {
    return sortSymbolMap[sort] || '▼▲'
  }
  renderSortButton (header) {
    return header.onSortClick &&
      (
        <button onClick={header.onSortClick || (() => {})}>
          {this.getSortSymbol(header.sort)}
        </button>
      )
  }
  renderHeaders () {
    return this.props.headers && (
      <thead>
        <tr>{this.props.headers.map((h, i) => (
          <th key={`t-header-${i}`}>
            {h.text}
            {this.renderSortButton(h)}
          </th>
        ))}</tr>
      </thead>
    )
  }
  renderRows () {
    return this.props.data && (
      <tbody>
        {this.props.data.map((row, ri) =>
          <tr key={`t-row-${ri}`}>
            {row.map((cell, ci) => <td key={`t-cell-${ri}-${ci}`}>{cell}</td>)}
          </tr>
        )}
      </tbody>
    )
  }
  render () {
    return (
      <table>
        { this.renderHeaders() }
        { this.renderRows() }
      </table>
    )
  }
}

module.exports = Table
