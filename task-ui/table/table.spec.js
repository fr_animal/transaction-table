/* eslint-env jest */
const React = require('react')
const { shallow } = require('enzyme')
const Table = require('./')

describe('<Table />', () => {
  it('Should render', () => {
    shallow(<Table />)
  })

  describe('Headers', () => {
    const getTableHeaders = table => table
      .find('table')
      .find('thead')
      .find('tr')
      .find('th')

    it('Should render', () => {
      const headers = [{ text: 'Name' }, { text: 'Amount' }, { text: 'Business' }]
      const table = shallow(<Table headers={headers} />)
      const th = getTableHeaders(table)

      expect(th).toHaveLength(headers.length)
      th.forEach((h, i) => {
        expect(h.prop('children')[0]).toEqual(headers[i].text)
      })
    })

    it('Should render sort buttons if sort func provided', () => {
      const onSortClick = () => {}
      const headers = [{ text: 'Name', sort: 1, onSortClick }, { text: 'Amount', sort: -1, onSortClick }, { text: 'Business' }]
      const table = shallow(<Table headers={headers} />)
      const th = getTableHeaders(table)

      const expectSymbol = {
        '1': '▲',
        '-1': '▼'
      }
      th.forEach((h, i) => {
        const button = h.find('button')
        if (headers[i].onSortClick) {
          const buttonText = h.find('button').text()
          const expectedSymbol = expectSymbol[headers[i].sort]
          expect(buttonText).toEqual(expectedSymbol || '▼▲')
        } else {
          expect(button).toHaveLength(0)
        }
      })
    })

    it('Should call sort onSortClick', () => {
      const headers = [
        { text: 'Name', onSortClick: jest.fn() },
        { text: 'Amount', onSortClick: jest.fn() },
        { text: 'Business', onSortClick: jest.fn() }
      ]

      const table = shallow(<Table headers={headers} />)
      const th = getTableHeaders(table)

      th.forEach((h, i) => {
        const onSortClick = h.find('button').prop('onClick')
        expect(headers[i].onSortClick).not.toHaveBeenCalled()
        onSortClick()
        expect(headers[i].onSortClick).toHaveBeenCalled()
      })
    })
  })

  describe('Data', () => {
    it('Should render rows of data', () => {
      const data = [
        ['1', 'Some company', 28.32],
        ['2', 'Some other company', 84.22],
        ['3', 'Some small business', 67.76]
      ]
      const table = shallow(<Table data={data} />)

      const rows = table
        .find('tbody')
        .find('tr')

      expect(rows).toHaveLength(data.length)

      rows.forEach((row, ri) => {
        const cells = row.find('td')
        expect(cells).toHaveLength(data[ri].length)
        cells.forEach((cell, ci) => {
          expect(cell.text()).toEqual(data[ri][ci].toString())
        })
      })
    })
  })
})
