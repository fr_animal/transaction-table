# Transaction Table App

# Build project

Run

```bash
yarn
```

or

```bash
npm i
```

in each of the directories
`./task-ui`, `./task-ui`, `./task-library`

Or just...

```bash
npm i && cd ./task-ui && npm i && cd ../task-library && npm i && cd ..
```

# Test

Run

```bash
yarn test
```

or

```bash
npm t
```

in each of the directories
`./task-ui`, `./task-ui`, `./task-library`

# Run

```bash
npm start
```
