module.exports = (order, transactions) =>
  [...transactions].sort((a, b) => a.amount < b.amount ? -order : order)
