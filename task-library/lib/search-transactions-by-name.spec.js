/* eslint-env jest */
const search = require('./search-transactions-by-name')

describe('Search transactions', () => {
  const dataSet = [
    { name: 'Vans Shoes' },
    { name: 'Van Rentals' },
    { name: 'Jimmy\'s Jelly Donuts' }
  ]

  it.each([
    [
      'Van',
      dataSet,
      [
        { name: 'Vans Shoes' },
        { name: 'Van Rentals' }
      ]
    ], [
      'Jelly',
      dataSet,
      [
        { name: 'Jimmy\'s Jelly Donuts' }
      ]
    ], [
      'y d',
      dataSet,
      [ { name: 'Jimmy\'s Jelly Donuts' } ]
    ], [
      'zz',
      dataSet,
      []
    ]
  ])('search(%s, %j)', (query, input, expected) => {
    expect(search(query, input)).toEqual(expected)
  })
})
