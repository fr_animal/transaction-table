module.exports = (name, transactions) =>
  transactions.filter(t => t.name.match(new RegExp(name, 'i')))
