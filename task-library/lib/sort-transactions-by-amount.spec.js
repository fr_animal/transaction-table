/* eslint-env jest */
const sort = require('./sort-transactions-by-amount')

describe('Sort transactions', () => {
  it('Should sort transactions by amount (ascending)', () => {
    const input = [
      { amount: 12.32 },
      { amount: 5.91 },
      { amount: 9.99 }
    ]

    const expected = [
      { amount: 5.91 },
      { amount: 9.99 },
      { amount: 12.32 }
    ]
    const sortAscending = sort.bind(null, 1)
    expect(sortAscending(input)).toEqual(expected)
  })

  it('Should sort transactions by amount (descending)', () => {
    const input = [
      { amount: 12.32 },
      { amount: 5.91 },
      { amount: 9.99 }
    ]

    const expected = [
      { amount: 12.32 },
      { amount: 9.99 },
      { amount: 5.91 }
    ]

    const sortDescending = sort.bind(null, -1)
    expect(sortDescending(input)).toEqual(expected)
  })
})
